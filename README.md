Created by: Tyler Spears and Brian Rich

Project Website: http://blistering-rascal-12-167146.use1.nitrousbox.com/

This program records data from a light sensor through an Arduino system, and displays it through the webpage located  in the webpage folder.
Note: The code found in this project does not contain the mysql database, so it is for analysis purposes only.
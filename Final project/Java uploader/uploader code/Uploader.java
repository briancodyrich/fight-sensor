import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.TimeZone;


public class Uploader {
	private String url; //the url to connect to
	private String infile; // the file to read from
	private String skipstring; //to continue if the database has duplicates
	private String success; //the success message from the server
	private String logfile = null; //optional log outputs (if you are not running in console)
  	private int sleeptime = 0; //how long to sleep between talking to server
	public Uploader() throws IOException
	{	
		//read in the config file
		BufferedReader reader = new BufferedReader(new FileReader("uploader.cfg"));
		String line = null;
		while ((line = reader.readLine()) != null) 
		{
			//options
			Scanner scan = new Scanner(line);
			switch (scan.next())
			{
				case "infile:":
					infile = scan.next();
					break;
				case "url:":
					url = scan.next();
					break;
				case "skipString:":
					skipstring = scan.next();
					break;
				case "success:":
					success = scan.next();
					break;
				case "logfile:":
					logfile = scan.next();
					break;
				case "sleepTime:":
					sleeptime = scan.nextInt();
					break;

			}
			scan.close();
		}
		reader.close();
	}
	public void Log(String str) throws IOException
	{
		//add to log file
		PrintWriter writer = new PrintWriter(new FileWriter(logfile, true));
		writer.println(str);
		writer.close();
	}
	
	public boolean TestResponse(StringBuffer buffer, String str, long data, long uTime) throws IOException
	{
		if (buffer.toString().equals(success)) //uccess
		{
			if (logfile != null)
			{
				Log("success time: " + str + " data: " + str + "/" + data);
			}
			System.out.println("success time: " + str + " data: " + str + "/" + data);
			return true;
		} else if (buffer.toString().contains(skipstring))  { //data exists on server
			if (logfile != null)
			{
				Log("dupe time: " + str + " data: " + uTime + "/" + data);
				Log(buffer.toString());
			}
			System.out.println("dupe time: " + str + " data: " + uTime + "/" + data);
			System.out.println(buffer.toString());
			return true;
		} else  { //all other errors
			if (logfile != null)
			{
				Log(buffer.toString());
			}
			System.out.println(buffer.toString());
		}
		return false;
	}
	
	
	public void Start() throws MalformedURLException, ProtocolException, IOException, ParseException, InterruptedException
	{
		int max = 0;
		for(;;)
		{
			BufferedReader reader = new BufferedReader(new FileReader(infile));
			String line = null;
			int counter = 0;
			while ((line = reader.readLine()) != null) 
			{
				counter++;
				if (counter > max) //if there is a new entry
				{
					Scanner scan = new Scanner(line);
					long uTime;
					int data;
					//convert time stamp to unix time
					SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
					String str = scan.next() + "-" + scan.next();
					date.setTimeZone(TimeZone.getDefault());
					uTime = date.parse(str).getTime() / 1000;
					
					//get the data
					data = scan.nextInt();
					//try to connect to server
					URL urlConnection = new URL(url);
					HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
					
					//send data to server
					connection.setRequestMethod("POST");
					String urlParameters = "time=" + uTime + "&data=" + data;
					connection.setDoOutput(true);
					DataOutputStream dataout = new DataOutputStream(connection.getOutputStream());
					dataout.writeBytes(urlParameters);
					dataout.flush();
					dataout.close();
					
					//get server response
					BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
					String input;
					StringBuffer response = new StringBuffer();
					while ((input = br.readLine()) != null) {
						response.append(input);
					}
					br.close();
					
					//write to console or logs
					if (TestResponse(response, str, data, uTime)) max++;
				    scan.close();
					
					//sleep on success if needed
				    if (sleeptime > 0)
				    {
				    	Thread.sleep(sleeptime);
				    }
				}
				
			}
			reader.close();
			
		}
	}

}

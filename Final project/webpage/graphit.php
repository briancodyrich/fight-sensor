<!DOCTYPE html>
<html lang="en">
  <head>
   <script type="text/javascript"
      src="dygraph-combined-dev.js"></script>
  <title>Here is your data</title>
  <meta charset="UTF-8">
      <style type="text/css">
    #graphdiv {
      position: absolute;
      left: 10px;
      right: 10px;
      top: 40px;
      bottom: 10px;
    }
    </style>
  </head>
  <body>

<div id="graphdiv"></div>
<script type="text/javascript">

  g = new Dygraph(
    document.getElementById("graphdiv"),
  

<?php
date_default_timezone_set("America/Chicago");
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "db";
$from = test_input($_POST["from"]);
$to = test_input($_POST["to"]);
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
echo "\"Time,Light_Level\\n\"";
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//use the from and to to query the database
$sql = "Select * from data WHERE TIME_STAMP >= " . $from . " AND TIME_STAMP <= " . $to . ";";
$data = $conn->query($sql);
if ($data->num_rows > 0) {

      //build the graph using dygraph
     while($row = $data->fetch_assoc()) {
      echo " + \n";
        echo "\"" . date('Y/m/d H:i', $row["TIME_STAMP"]). "," . $row["LIGHT_DATA"]  . "\\n\"";
     }
}
echo "\n);";

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
?>
     
</script>
</body>
</html>
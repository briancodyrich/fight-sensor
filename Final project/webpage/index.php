<!DOCTYPE html>
<html lang="en">
  <head>
  <title>Graphing some things</title>
  <meta charset="UTF-8">
  </head>
  <body>

<?php
date_default_timezone_set("America/Chicago");
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "db";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//set up form
echo "<form name=\"g\" method=\"post\"" . " action=\"graphit.php\" target=\"_blank\" >";


//build dropdown 1
$sql = "Select * from data;";
$data = $conn->query($sql);
if ($data->num_rows > 0) {

echo "<select name=\"from\">";
     while($row = $data->fetch_assoc()) {
        echo "<option value=\"" . $row["TIME_STAMP"] . "\">" . date('m/d H:i', $row["TIME_STAMP"]) . "</option>";
       //  echo date('m/d/Y H:i', $row["TIME_STAMP"]) . "   " . $row["TIME_STAMP"] . " - ". $row["LIGHT_DATA"]. "<br>";
     }
  
echo "</select> Until ";
}
//build dropdown 2
$sql = "Select * from data;";
$data = $conn->query($sql);
if ($data->num_rows > 0) {

echo "<select name=\"to\">";
     while($row = $data->fetch_assoc()) {
        echo "<option value=\"" . $row["TIME_STAMP"] . "\">" . date('m/d H:i', $row["TIME_STAMP"]) . "</option>";
       //  echo date('m/d/Y H:i', $row["TIME_STAMP"]) . "   " . $row["TIME_STAMP"] . " - ". $row["LIGHT_DATA"]. "<br>";
     }
  
echo "</select><br>";
}

echo "<input type=\"submit\" value=\"Graph this\">";
echo "</form>";
$conn->close();

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
?>

</body>
</html>
/*
Created by: Tyler Spears
Thanks to Brian Rich for the initial version

This simple sketch reads a photoresistor on pin A0 (analog 0)
and prints the value to the console every minute.
*/

#define ANALOGPIN 0
#define DELAYTIME 60000 //one minute in milliseconds


void setup(void) {
  
  //sets up the serial connection with a baud of 9600
  Serial.begin(9600);   
}

void loop(void) {
  
  //main code is placed in an infinite while loop so
  //  the loop() function is not infinitely called, 
  //  reducing overhead slightly
  while(1){
    
  Serial.println(analogRead(ANALOGPIN));
  
  //delays program for the set amount of milliseconds
  delay(DELAYTIME); 
  }
  
}

